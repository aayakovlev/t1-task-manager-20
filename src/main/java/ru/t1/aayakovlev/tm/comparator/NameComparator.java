package ru.t1.aayakovlev.tm.comparator;

import ru.t1.aayakovlev.tm.model.HasName;

import java.util.Comparator;

public enum NameComparator implements Comparator<HasName> {

    INSTANCE;

    @Override
    public int compare(final HasName o1, final HasName o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getName() == null || o2.getName() == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}

package ru.t1.aayakovlev.tm.model;

public abstract class AbstractUserOwnedModel extends AbstractExtendedModel {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}

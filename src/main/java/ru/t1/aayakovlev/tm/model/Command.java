package ru.t1.aayakovlev.tm.model;

import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface Command {

    String getArgument();

    String getDescription();

    String getName();

    void execute() throws AbstractException;

}

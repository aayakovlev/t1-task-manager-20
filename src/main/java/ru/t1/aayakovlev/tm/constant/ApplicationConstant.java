package ru.t1.aayakovlev.tm.constant;

public final class ApplicationConstant {

    public final static int EMPTY_ARRAY_SIZE = 0;

    public final static int FIRST_ARRAY_ELEMENT_INDEX = 0;

    private ApplicationConstant() {
    }

}

package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove project by id.";

    public static final String NAME = "project-remove-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        getProjectService().findById(id);
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, id);
    }

}

package ru.t1.aayakovlev.tm.command.system;

import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.service.CommandService;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Override
    public Role[] getRoles() {
        return null;
    }

    protected CommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}

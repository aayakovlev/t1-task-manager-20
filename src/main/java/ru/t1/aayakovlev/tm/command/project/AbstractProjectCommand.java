package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected ProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected ProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

}

package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Task create.";

    public static final String NAME = "task-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }

}

package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Project;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove project by index.";

    public static final String NAME = "project-remove-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final String userId = getUserId();
        final Project project = getProjectService().findByIndex(userId, index);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}

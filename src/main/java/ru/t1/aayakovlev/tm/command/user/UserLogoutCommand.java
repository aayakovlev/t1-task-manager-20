package ru.t1.aayakovlev.tm.command.user;

import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;


public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Logout user session.";

    public static final String NAME = "logout";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Update task by index.";

    public static final String NAME = "task-update-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final String userId = getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }

}

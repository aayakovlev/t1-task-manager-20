package ru.t1.aayakovlev.tm.command.system;

public final class SystemExitCommand extends AbstractSystemCommand {

    public static final String DESCRIPTION = "Exit program.";

    public static final String NAME = "exit";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}

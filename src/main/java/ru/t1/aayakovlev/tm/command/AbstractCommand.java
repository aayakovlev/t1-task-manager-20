package ru.t1.aayakovlev.tm.command;

import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.model.Command;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

public abstract class AbstractCommand implements Command {

    protected ServiceLocator serviceLocator;

    protected AuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public abstract Role[] getRoles();

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String getUserId() throws UserNotLoggedException {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}

package ru.t1.aayakovlev.tm.exception.auth;

public class FailedLoginException extends AbstractAuthException {

    public FailedLoginException() {
        super("Error! Failed to login with entered credentials...");
    }

}

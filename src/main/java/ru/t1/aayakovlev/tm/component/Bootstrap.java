package ru.t1.aayakovlev.tm.component;

import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.command.project.*;
import ru.t1.aayakovlev.tm.command.system.*;
import ru.t1.aayakovlev.tm.command.task.*;
import ru.t1.aayakovlev.tm.command.user.*;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aayakovlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.*;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.EMPTY_ARRAY_SIZE;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class Bootstrap implements ServiceLocator {

    private static final CommandRepository commandRepository = new CommandRepositoryImpl();

    private static final CommandService commandService = new CommandServiceImpl(commandRepository);

    private static final LoggerService loggerService = new LoggerServiceImpl();

    private static final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    private static final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    private static final TaskRepository taskRepository = new TaskRepositoryImpl();

    private static final TaskService taskService = new TaskServiceImpl(taskRepository);

    private static final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    private static final UserRepository userRepository = new UserRepositoryImpl();

    private static final UserService userService = new UserServiceImpl(userRepository);

    private static final AuthService authService = new AuthServiceImpl(userService);

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompeteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowAllCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new SystemArgumentListCommand());
        registry(new SystemCommandListCommand());
        registry(new SystemExitCommand());
        registry(new SystemShowAboutCommand());
        registry(new SystemShowInfoCommand());
        registry(new SystemShowVersionCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowAllCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdateProfileCommand());
    }

    @Override
    public AuthService getAuthService() {
        return authService;
    }

    @Override
    public CommandService getCommandService() {
        return commandService;
    }

    @Override
    public LoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    private void initData() throws AbstractException {
        final User first = userService.create("first", "first", "first@first.com");
        final User second = userService.create("second", "second", "second@second.com");
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.save(first.getId(), new Project("project-1", "description"));
        projectService.save(first.getId(), new Project("project-2", Status.COMPLETED));
        projectService.save(second.getId(), new Project("project-3", Status.IN_PROGRESS));
        projectService.save(admin.getId(), new Project("project-4", Status.COMPLETED));

        taskService.save(first.getId(), new Task("task-1", "description"));
        taskService.save(first.getId(), new Task("task-2", Status.IN_PROGRESS));
        taskService.save(second.getId(), new Task("task-3", Status.COMPLETED));
        taskService.save(second.getId(), new Task("task-4", Status.IN_PROGRESS));
        taskService.save(first.getId(), new Task("task-5", Status.COMPLETED));
        taskService.save(admin.getId(), new Task("task-6", "description"));
    }

    private void initLogger() {
        loggerService.info(
                "___________              __                                                        \n" +
                "\\__    ___/____    _____|  | __   _____ _____    ____ _____     ____   ___________ \n" +
                "  |    |  \\__  \\  /  ___/  |/ /  /     \\\\__  \\  /    \\\\__  \\   / ___\\_/ __ \\_  __ \\\n" +
                "  |    |   / __ \\_\\___ \\|    <  |  Y Y  \\/ __ \\|   |  \\/ __ \\_/ /_/  >  ___/|  | \\/\n" +
                "  |____|  (____  /____  >__|_ \\ |__|_|  (____  /___|  (____  /\\___  / \\___  >__|   \n" +
                "               \\/     \\/     \\/       \\/     \\/     \\/     \\//_____/      \\/");
        Runtime.getRuntime().addShutdownHook(new Thread(
                () -> loggerService.info("*** APPLICATION SHUTTING DOWN ***"))
        );
    }

    private boolean processArguments(final String[] arguments) throws AbstractException {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return false;
        final String argument = arguments[FIRST_ARRAY_ELEMENT_INDEX];
        return processArgument(argument);
    }

    private boolean processArgument(final String argument) throws AbstractException {
        if (argument == null || argument.isEmpty()) return false;
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
        return true;
    }

    private void processCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) throws AbstractException {
        if (processArguments(args)) return;
        initData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command: ");
                final String command = nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final AbstractException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}

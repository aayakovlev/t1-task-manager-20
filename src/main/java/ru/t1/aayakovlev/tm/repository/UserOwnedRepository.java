package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface UserOwnedRepository<M extends AbstractUserOwnedModel> extends BaseRepository<M> {

    void clear(final String userId) throws UserIdEmptyException;

    int count(final String userId) throws UserIdEmptyException;

    boolean existsById(final String userId, final String id) throws AbstractFieldException;

    List<M> findAll(final String userId) throws UserIdEmptyException;

    List<M> findAll(final String userId, final Comparator<M> comparator) throws UserIdEmptyException;

    M findById(final String userId, final String id) throws AbstractFieldException;

    M findByIndex(final String userId, final Integer index) throws AbstractFieldException;

    M remove(final String userId, final M model) throws AbstractException;

    M removeById(final String userId, final String id) throws AbstractException;

    M removeByIndex(final String userId, final Integer index) throws AbstractFieldException;

    M save(final String userId, final M model) throws UserIdEmptyException;

}

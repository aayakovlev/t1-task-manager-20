package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepositoryImpl extends AbstractUserOwnedRepository<Task> implements TaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        return save(userId, task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return save(userId, task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (!task.getProjectId().equals(projectId)) continue;
            if (!task.getUserId().equals(userId)) continue;
            projectTasks.add(task);
        }
        return projectTasks;
    }

}

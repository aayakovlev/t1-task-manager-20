package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;

public final class ProjectRepositoryImpl extends AbstractUserOwnedRepository<Project> implements ProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return save(project);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return save(project);
    }

}

package ru.t1.aayakovlev.tm.service;

public interface LoggerService {

    void info(final String message);

    void command(final String message);

    void debug(final String message);

    void error(final Exception e);

}
